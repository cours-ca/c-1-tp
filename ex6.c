#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>

int v = 0;
int nbLoops = 1000000;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_spinlock_t spinlock;
int ret;


void * increment() {
  for (size_t i = 0; i < nbLoops; i++)
  {
    /* code */
    // pthread_mutex_lock(&mutex);
    pthread_spin_lock(&spinlock);
    v++;
    pthread_spin_unlock(&spinlock);
    // pthread_mutex_unlock(&mutex);
  }
}

enum { NS_PER_SECOND = 1000000000 };

void substractTimespec(struct timespec t1, struct timespec t2, struct timespec *td)
{
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += NS_PER_SECOND;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= NS_PER_SECOND;
        td->tv_sec++;
    }
}

int main(int argc, char * argv[]) {
  ret = pthread_spin_init(&spinlock, 0);

  int nbThreads;

  if (argc > 1) 
  {
    sscanf(argv[1], "%d", &nbThreads);
  } else {
    nbThreads = 5;
  }

  struct timespec start, stop, diff;
  double nsDiff;

  clock_gettime(CLOCK_REALTIME, &start);

  pthread_t threadList[nbThreads];

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_create(&threadList[i], NULL, &increment, NULL);
  }

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], NULL);
  }

  clock_gettime(CLOCK_REALTIME, &stop);

  substractTimespec(start, stop, &diff);

  double executionTime = (int)diff.tv_sec * NS_PER_SECOND + (double)diff.tv_nsec;

  printf("Execution time : %f ns\n", executionTime);

  printf("The final value is %d, it should have been %d\n", v, nbLoops * nbThreads);
  
}