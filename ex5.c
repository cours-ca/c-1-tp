#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>


struct ThreadArgs {
  int start;
  int nb;
  int * arr;
};

enum { NS_PER_SECOND = 1000000000 };

void substractTimespec(struct timespec t1, struct timespec t2, struct timespec *td)
{
    td->tv_nsec = t2.tv_nsec - t1.tv_nsec;
    td->tv_sec  = t2.tv_sec - t1.tv_sec;
    if (td->tv_sec > 0 && td->tv_nsec < 0)
    {
        td->tv_nsec += NS_PER_SECOND;
        td->tv_sec--;
    }
    else if (td->tv_sec < 0 && td->tv_nsec > 0)
    {
        td->tv_nsec -= NS_PER_SECOND;
        td->tv_sec++;
    }
}

double getSleepTime() {
  struct timespec start, stop, diff;
  double nsDiff;

  clock_gettime(CLOCK_REALTIME, &start);

  // sleep(1);

  clock_gettime(CLOCK_REALTIME, &stop);

  substractTimespec(start, stop, &diff);

  return (int)diff.tv_sec * NS_PER_SECOND + (double)diff.tv_nsec;
}

double avgArray(int arraySize, double arr[]) {
  double total = 0.0;

  for (size_t i = 0; i < arraySize; i++)
  {
    total += arr[i];
  }

  return total / arraySize;
  
}

void * generateArray(int arraySize, int * arr) {
  srand(pthread_self());

  for (size_t i = 0; i < arraySize; i++)
  {
    arr[i] = 1;
  }
}

int arraySum(int arraySize,int arr[]) {
  int res = 0;

  for (size_t i = 0; i < arraySize; i++)
  {
    res += arr[i];
  }
  
  return res;
}

int calcArray(struct ThreadArgs * args) {
  int res = 0;

  for (int i = 0; i < args->nb; i++)
  {
    int index = args->start + i;
    res += args->arr[index];
  }

  return res;
}

int main(int argc, char * argv[]) {
  int nbThreads;
  int arraySize;

  if (argc > 2) 
  {
    sscanf(argv[1], "%d", &nbThreads);
    sscanf(argv[2], "%d", &arraySize);
  } else {
    nbThreads = 5;
    arraySize = 10;
  }

  int totalArray[arraySize];

  generateArray(arraySize, totalArray);

  pthread_t threadList[nbThreads];

  struct ThreadArgs threadArgs[nbThreads];

  int nbElementsPerArray = arraySize / nbThreads;

  struct timespec startSeq, stopSeq, diffSeq, startThr, stopThr, diffThr;

  clock_gettime(CLOCK_REALTIME, &startThr);

  for (size_t i = 0; i < nbThreads; i++)
  {
    int nbElements = nbElementsPerArray;

    if (i == nbThreads - 1) {
      nbElements = nbElementsPerArray + arraySize % nbThreads;
    }

    threadArgs[i].nb = nbElements;
    threadArgs[i].start = i * nbElements;
    threadArgs[i].arr = totalArray;

    pthread_create(&threadList[i], NULL, calcArray, &threadArgs[i]); 
  }

  int retValues[nbThreads];

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], &retValues[i]);
  }

  clock_gettime(CLOCK_REALTIME, &stopThr);

  clock_gettime(CLOCK_REALTIME, &startSeq);

  int seqRes = arraySum(arraySize, totalArray);

  clock_gettime(CLOCK_REALTIME, &stopSeq);

  substractTimespec(startSeq, stopSeq, &diffSeq);

  double sequentialDiff = (int)diffSeq.tv_sec * NS_PER_SECOND + (double)diffSeq.tv_nsec;

  substractTimespec(startThr, stopThr, &diffThr);

  double threadedDiff = (int)diffThr.tv_sec * NS_PER_SECOND + (double)diffThr.tv_nsec;

  printf("Total result threaded : %d\n", arraySum(nbThreads, retValues));
  printf("Threaded time : %f ns\n", threadedDiff);
  printf("Total result sequential : %d\n", seqRes);
  printf("Sequential time : %f ns\n", sequentialDiff);

  
  
}