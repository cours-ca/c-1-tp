#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>

struct ThreadArgs {
  int threadId;
  int calcResults;
};

void * myprintf(int threadId, const char * str, ...) {
  char strToPrint[512];

  va_list args;
  va_start(args, str);

  vsnprintf(strToPrint, sizeof(strToPrint), str, args);

  va_end(args);

  printf("{%d} %s\n", threadId, strToPrint);
}

void * helloWorld(struct ThreadArgs * args) 
{
  myprintf(args->threadId, "Hello world");
  
  srand(pthread_self());

  int * rnd = (rand() % 5) + 1;

  sleep(rnd);

  myprintf(args->threadId, "I slept for %d seconds", rnd);

  sleep(1);
  
  myprintf(args->threadId, "Bye !");

  args->calcResults = args->threadId * args->threadId;

  return args->calcResults;
}

int main(int argc, char * argv[])
{
  int nbThreads;

  if (argc > 1) {
    sscanf(argv[1], "%d", &nbThreads);
  } else {
    nbThreads = 5;
  }

  pthread_t threadList[nbThreads];

  struct ThreadArgs threadArgs[nbThreads];

  int retValues[nbThreads];

  for (size_t i = 0; i < nbThreads; i++)
  {
    threadArgs[i].threadId = i;

    pthread_create(&threadList[i], NULL, &helloWorld, &threadArgs[i]);
  }

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], &retValues[i]);

    printf("Thread %d answered %d and %d\n", i, threadArgs[i].calcResults, retValues[i]);
  }

  return 0;
}