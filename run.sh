#!/bin/bash

fileToCompile="$1"

# programToRun=$(echo $fileToCompile| cut -d'.' -f 1).out

shift

# gcc -pthread $fileToCompile

mpicc -pedantic -Wall -Wextra -o a.out $fileToCompile

./a.out $@