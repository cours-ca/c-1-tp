#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>

int v = 0;
int nbLoops = 1000000;

void * increment() {
  v++;

  for (size_t i = 0; i < nbLoops; i++)
  {
    /* code */
    
    v++;
  }
}

int main(int argc, char * argv[]) {
  int nbThreads;

  if (argc > 1) 
  {
    sscanf(argv[1], "%d", &nbThreads);
  } else {
    nbThreads = 5;
  }

  pthread_t threadList[nbThreads];

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_create(&threadList[i], NULL, &increment, NULL);
  }

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], NULL);
  }

  printf("The final value is %d, it should have been %d\n", v, nbLoops * nbThreads);
  
}