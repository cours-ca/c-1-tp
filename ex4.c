#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>

struct ThreadArgs {
  int start;
  int nb;
  int * arr;
};

void * generateArray(int arraySize, int * arr) {
  srand(pthread_self());

  for (size_t i = 0; i < arraySize; i++)
  {
    arr[i] = 1;
  }
}

int arraySum(int arraySize,int arr[]) {
  int res = 0;

  for (size_t i = 0; i < arraySize; i++)
  {
    res += arr[i];
  }
  
  return res;
}

int calcArray(struct ThreadArgs * args) {
  int res = 0;

  for (int i = 0; i < args->nb; i++)
  {
    int index = args->start + i;
    res += args->arr[index];
  }

  return res;
}

int main(int argc, char * argv[]) {
  int nbThreads;
  int arraySize;

  if (argc > 2) 
  {
    sscanf(argv[1], "%d", &nbThreads);
    sscanf(argv[2], "%d", &arraySize);
  } else {
    nbThreads = 5;
    arraySize = 10;
  }

  int totalArray[arraySize];

  generateArray(arraySize, totalArray);

  pthread_t threadList[nbThreads];

  struct ThreadArgs threadArgs[nbThreads];

  int nbElementsPerArray = arraySize / nbThreads;

  for (size_t i = 0; i < nbThreads; i++)
  {
    int nbElements = nbElementsPerArray;

    if (i == nbThreads - 1) {
      nbElements = nbElementsPerArray + arraySize % nbThreads;
    }

    threadArgs[i].nb = nbElements;
    threadArgs[i].start = i * nbElements;
    threadArgs[i].arr = totalArray;

    pthread_create(&threadList[i], NULL, calcArray, &threadArgs[i]); 
  }

  int retValues[nbThreads];

  for (size_t i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], &retValues[i]);
  }

  printf("Total result sequential : %d\n", arraySum(arraySize, totalArray));
  printf("Total result threaded : %d\n", arraySum(nbThreads, retValues));
}