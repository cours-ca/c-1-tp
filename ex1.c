#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void * helloWorld() 
{
  printf("Hello world\n");
  sleep(10);
  printf("Bye !\n");

  return NULL;
}

int main(int argc, char * argv[])
{
  int nbThreads;

  if (argc > 1){
    sscanf(argv[1], "%d", &nbThreads);
  } else {
    nbThreads = 5;
  }

  pthread_t threadList[nbThreads];

  for (int i = 0; i < nbThreads; i++)
  {
    pthread_create(&threadList[i], NULL, &helloWorld, NULL);
  }

  for (int i = 0; i < nbThreads; i++)
  {
    pthread_join(threadList[i], NULL);
  }

  return 0;
}